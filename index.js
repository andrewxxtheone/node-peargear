global.Class = require("ee-class");
global.Express = require("express");
global._ = require('underscore');
global.async = require('async');
global.Sequelize = global._sql = require("sequelize");

require("./src/controller.js");
require("./src/action.js");

var winston = require('winston');

global.directoryWalker = function(path, each, end) {
	var _walker = require('walk').walk(path, { followLinks: false });
	_walker.on('file', each);
	_walker.on('end', end);
};

global.PearGear = new Class({

	//consts-start-
	CONST_PATH_XHR: 0,
	CONST_PATH_POST: 1,
	CONST_PATH_GET: 2,
	CONST_PATH_SOCKET: 4,
	//consts-end-

	express: null,
	server: null,
	config: {},
	routes: {},
	controllers: {},
	logger: null,
	sequelize: null,
	models: {},
	hooks: {},

	init: function(config) {
		var that = this;
		var base_config = {
			base_path: null,
			controller_dir: null,
			model_dir: null,
			logs_dir: null,
			view_dir: null,
			public_dir: null,
			domain: null,
			port: 80,
			db_dns: null,
			websocket: {
				on: false,
				port: 8080,
				path: null,
			},
			log_level: 3,
		};

		this.config = _.extend(base_config, config);
		this.express = Express();

		this.express.use('/static', Express.static(require('path').resolve(that.config.base_path, that.config.public_dir)));
		this.express.set('views', require('path').resolve(that.config.base_path, that.config.view_dir));
		this.express.set('view engine', 'jade');

		this.addHook("before", function() {});

		var Router = require('reversable-router');
		var router = new Router();
		router.extendExpress(this.express);
		router.registerAppHelpers(this.express);

		this.express.use(function (req, res, next_mdw) {
			if(that.hooks.hasOwnProperty('after')) {
				if( typeof that.hooks["after"] == "object" ) {
					for(var i in that.hooks["after"]) {
						res.on('finish', that.hooks['after'][i].bind(null, req, res));
					}
				}
				else
					res.on('finish', that.hooks['after'].bind(null, req, res));
			}

			if(that.hooks.hasOwnProperty('before')) {
				if( typeof that.hooks["before"] == "object" ) {
					for(var i in that.hooks["before"]) {
						that.hooks['before'][i].apply(null, [req, res]);
					}
				}
				else
					that.hooks['before'].apply(null, [req, res]);
			}
			next_mdw();
		});

		global.logger = this.logger = new (winston.Logger)({
			transports: [
				new (winston.transports.Console)()
			]
		});

		if(this.config.logs_dir) {
			var startupTime = new Date();
			logger.add(winston.transports.File, { filename: require('path').resolve(this.config.base_path, this.config.logs_dir)+"/"+startupTime.getFullYear()+"_"+startupTime.getMonth()+"_"+startupTime.getDay()+"_"+startupTime.getHours()+"_"+startupTime.getMinutes()+"_"+startupTime.getSeconds()+".log" });
		}
	},

	addHook: function(key, fnc) {
		if( this.hooks.hasOwnProperty(key) && typeof this.hooks[key] !== "object" ) {
			var tmp = this.hooks[key];
			this.hooks[key] = [];
			this.hooks[key].push(tmp);
			this.hooks[key].push(fnc);
		}
		else if( this.hooks.hasOwnProperty(key) ) {
			this.hooks[key].push(fnc);
		}
		else
			this.hooks[key] = fnc;
	},

	startup: function() {
		var that = this;

		/* registering global functions that uses peargear instance... */
		GLOBAL._x = function(name, fnc, options) {
			that.register_path(that.CONST_PATH_XHR, that.getOnloadingControllerName(), name, options || {}, fnc);
		};
		GLOBAL._g = function(name, fnc, options) {
			that.register_path(that.CONST_PATH_GET, that.getOnloadingControllerName(), name, options || {}, fnc);
		};
		GLOBAL._p = function(name, fnc, options) {
			that.register_path(that.CONST_PATH_POST, that.getOnloadingControllerName(), name, options || {}, fnc);
		};
		GLOBAL._s = function(name, fnc, options) {
			that.register_path(that.CONST_PATH_SOCKET, that.getOnloadingControllerName(), name, options || {}, fnc);
		};
		GLOBAL._m = function(name, definition, options) {
			if(definition == null && definition !== {})
				return that.models[name];
			that.register_model(name, definition);
		};

		async.waterfall([
			function connectToDatabase(next) {
				that.sequelize = new Sequelize(that.config.db_dns, {});
				that.sequelize.sync().done(function(err) {
					if(err)
						next(err);
					else
						next();
				});
			},
			function modelLoader(next) {
				var model_countr = 0;
				/* load peargear's route's model first */
				require(__dirname+"/src/model_routes.js");

				if(that.config.model_dir == null) {
					logger.info('Model direcotry is not provided, assumed there are no models, only peargear routes is loaded!')
					return next();
				}

				/* iterate through user-defined models if any*/				
				directoryWalker(
					require('path').resolve(that.config.base_path, that.config.model_dir),
					function(root, stat, next_itr) {
						if( stat.name == 'relations.js' )
							return next_itr(); //relations must be loaded last!
						
						require(root+'/'+stat.name);
						
						model_countr++;

						next_itr();
					},
					function() {
						/* now that we loaded all user-defined models, we need to ensure to load relations.js */
						if (require('fs').existsSync(require('path').resolve(that.config.base_path, that.config.model_dir)+"/relations.js")) { // or fs.existsSync
							require(require('path').resolve(that.config.base_path, that.config.model_dir)+"/relations.js");
						}

						/* after that we must truncate peargear's route table */
						_m('Route').sync({force: true}).done(function(e) {

							/* now update database tables according to models */
							that.sequelize.sync().done(function(err) {
								if(err) {
									next(err);
								} else {
									logger.info('Models were loaded!');
									next();
								}
							});
						});
					}
				);
			},
			function controllerLoader(next) {
				directoryWalker(
					require('path').resolve(that.config.base_path, that.config.controller_dir),
					function(root, stat, next_itr) {
						that.__onLoadingController = stat.name.replace(/\\/g, "").replace(/\//g, "").replace(/\.js/g, "");
						that.controllers[that.getOnloadingControllerName()] = new Controller(that.getOnloadingControllerName());
					    require(root+'/'+stat.name);
					    next_itr();
					},
					function() {
					    logger.info('Controllers were loaded!');

					    that.server = that.express.listen(that.config.port, function() {
							logger.info('Listening on port %d', that.server.address().port);
							next();
						});
					}
				);
			}
		], function(err) {
			if(err)
				logger.error(err);
		});
	},

	getOnloadingControllerName: function() { return this.__onLoadingController; },

	register_path: function(type, controller, name, options, fnc) {
		var base_path_options = {
			path: '/',
			name: controller+'.'+name,
			view_file: require('path').resolve(this.config.base_path, this.config.view_dir)+"/"+controller+"/"+name
		};

		if( controller != 'index' ) {
			base_path_options += controller;

			if( name != 'index' ) {
				base_path_options += '/'+name;
			}
		} else {
			if( name != "index" ) {
				base_path_options += controller+'/'+name;
			}
		}

		options = _.extend(base_path_options, options);
		
		this.controllers[controller].addAction(new Action(name, options));

		_m('Route').build({action: name, controller: controller, url: options.path}).save();

		var fnc_alias = function(req, res) {
			res.r = function(args) {
				if(!args)
					args = {};

				res.render(options.view_file, args, function(err, html) {
					console.log(err);
					res.send(html);
				});
			};

			fnc.apply({m: _m}, [req, res]);
		};


		switch(type) {
			case this.CONST_PATH_POST:
				this.express.post(options.path, options.name, fnc_alias);
			break;
			case this.CONST_PATH_GET:
				this.express.get(options.path, options.name, fnc_alias);
			break;
		}
		//logger.info(controller+"/"+name, "is loaded!");
	},

	register_model: function(name, definition, options) {
		this.models[name] = this.sequelize.define(name, definition, options || {});
	},
});


//test
/*var pg = new PearGear({db_dns: 'mysql://test:pass@localhost:3306/test_db', controller_dir: 'ctrls', logs_dir: "logs", port: 8080});
pg.startup();*/