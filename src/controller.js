
global.Controller = new Class({

	name: null,
	actions: {},

	init: function(name) {
		this.name = name;
	},

	addAction: function(name, action) {
		this.actions[name] = action;
	},

	dispatchRequest: function(request, response) {
		console.log(request);
	},

	createUrl: function(action, params) {
		return this.actions[action].createUrl(params);
	},
});
